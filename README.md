# PROMISE.scrub <img src="man/figures/logo.png" align="right" height=140/>

The goal of PROMISE.scrub is to provide functions that help with cleaning and
preparing the PROMISE dataset for eventual use in analyses and research
projects. There are several of these packages that all work to make it easier
to handle the different needs of preparing the PROMISE dataset, at least from a
data management point of view, to be trustworthy and of sufficient quality control.

# Installation

You can install PROMISE.scrub from GitLab with:

```R
# install.packages("pak")
pak::pak("gitlab::promise-cohort/PROMISE.scrub")
```

# Usage

This package and it's functions are used in the main PROMISE package to prepare 
the data into a tidy format. Please see the associated PROMISE documentation
for more details about usage.
