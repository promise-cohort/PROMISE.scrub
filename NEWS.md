# PROMISE.scrub 0.0.8

## Bug fix

- `fix_date()` and `convert_to_date()` were converting ISO8601 (`YYYY-MM-DD`) date formats from the original date field into missing.
- `spread_over_visits()` was returning a data frame that was filling values down too many rows (e.g. adding values into another participant's row). Fixed.

# PROMISE.scrub 0.0.7

## Updates

- Removed deprecated `_()` functions, like `fill_()` and `arrange_()`.

## Fixes

- Added argument to `full_join()` to explicit set a "many to many" relationship when joining.

# PROMISE.scrub 0.0.6

## Updates

* Removed travis and codecov, since they don't work anymore.
* Switched URLs to the GitLab PROMISE organization rather than GitHub.

## Bug fixes

* The assertive package has been archived on CRAN, so is not available anymore.
* Fix deprecated dplyr functions (e.g. `group_by_()` and `_at()`).

# PROMISE.scrub 0.0.5

## Bug fixes

* Minor fixes and updates based on newer versions of dependency packages.

# PROMISE.scrub 0.0.4

## Hotfix

* Fix a bug in the `combine_datasets` that silently dropped duplicate columns.

# PROMISE.scrub 0.0.3

* Added function to remove duplicates (`scr_duplicates`).
* Added code coverage.
* Added website infrastructure

# PROMISE.scrub 0.0.2

* Added `average_variables` function to average out variables with multiple
measurements.
* Spaces removed from column names when renaming them.

# PROMISE.scrub 0.0.1

* Added unit tests for the scrubbing functions.
* Moved many of the scrubbing and cleaning functions from the original PROMISE
package into this one.
* Added a `NEWS.md` file to track changes to the package.
